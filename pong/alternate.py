# For machine learning AI
import numpy as np
from sklearn.ensemble import RandomForestClassifier


# 1 for up/above
# 2 for down/below
# col1: Ball position x
# col2: Ball above or below paddle
# col3: Ball velocity y, 1 for up, 0 for down
# Input dataset
X = np.array([[1, 0, 1],
             [0, 0, 1],
             [1, 0, 0],
             [0, 0, 0],
             [1, 1, 1],
             [0, 1, 1],
             [1, 1, 0],
             [0, 1, 0]])

# Output dataset
# 0 for down 1 for up
# 2 for neither
y = np.array([[0, 0, 0, 0, 1, 1, 1, 1]]).T

# seed random numbers to make calculation
# ... deterministic (just a good practice)
np.random.seed(1)

clf = RandomForestClassifier(max_depth=2, random_state=0)
clf.fit(X, y)

'''
# Machine Learning
# Input
# ... ballPosition
# ... ballVelocity
# ... paddlePositionR
# Output
# ... Move up or down
# More complicated
# ... Adjust paddle height to make abs(ball velocity) higher so opponent loses more
#
# Ball position x
# Ball above or below paddle
# Ball velocity y, 1 for up, 0 for down
if self.ballPosition[0] < self.width / 2:
    ballPositionX = 0
else:
    ballPositionX = 1
if self.ballPosition[1] > self.paddlePositionR[1]:
    ballAbove = 1
else:
    ballAbove = 0
if self.ballVelocity[1] < 0:
    velocity = 1
else:
    velocity = 0

upOrDown = clf.predict([[ballPositionX, ballAbove, velocity]])
if upOrDown[0] == 1:
    self.paddleVelocityR = 3
else:
    self.paddleVelocityR = -3

if self.paddlePositionR[1] > self.halfPaddleHeight and self.paddlePositionR[1] < self.height - self.halfPaddleHeight:
    self.paddlePositionR[1] += self.paddleVelocityR
# So paddle doesn't get stuck at the top of the screen
elif self.paddlePositionR[1] == self.halfPaddleHeight and self.paddleVelocityR > 0:
    self.paddlePositionR[1] += self.paddleVelocityR
# So paddle doesn't get stuck at the bottom of the screen
elif self.paddlePositionR[1] >= self.height - self.halfPaddleHeight and self.paddleVelocityR < 0:
    self.paddlePositionR[1] += self.paddleVelocityR
'''
