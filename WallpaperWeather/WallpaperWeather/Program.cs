﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using Contentful.Core;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net;
using System.Linq;
using Newtonsoft.Json.Linq;


namespace WallpaperWeather
{
    public class Program
    {
        private static readonly WebClient webClient = new WebClient();
        private static readonly HttpClient client = new HttpClient();
        private static readonly Random random = new Random();
        private static string zipcode;

        public static void Main(string[] args)
        {
            zipcode = args.Length > 0 ? args[0] : "63131";
            RunTeleprompter().Wait();
        }

        private static async Task RunTeleprompter()
        {
            Console.WriteLine("The Wallpaper Weather app is now running...\nPress `q` to quit\n");
            var requestWallpaper = RequestWallpaper();
            var escapeTask = GetInput();

            await Task.WhenAny(requestWallpaper, escapeTask);
        }

        private static async Task RequestWallpaper()
        {
            while (true)
            {
                string uri = $"https://api.aerisapi.com/forecasts/{zipcode}?client_id={Secrets.clientId}&client_secret={Secrets.clientSecret}";
                string responseForecast = await client.GetStringAsync($"{uri}");
                dynamic responseForecastJSON = JsonConvert.DeserializeObject(responseForecast);

                string weatherForecast = responseForecastJSON.response[0].periods[0].weather;
                string[] weather = weatherForecast.ToLower().Split(' ');

                Console.WriteLine("\nDownloading image and setting it to your wallpaper...\n");
                var assets = $"https://cdn.contentful.com/spaces/{Secrets.spaceId}/assets?access_token={Secrets.contentDelivery}";
                string assetsResponse = await client.GetStringAsync($"{assets}");
                dynamic assetsResponseJSON = JsonConvert.DeserializeObject(assetsResponse);

                int total = (int)assetsResponseJSON.total;
                List<string> weatherMatchesList = new List<string>();

                for (int i = 0; i < total; i++)
                {
                    string description = assetsResponseJSON.items[i].fields.description;
                    string[] descriptionTags = description.Split();
                    string fileUri = assetsResponseJSON.items[i].fields.file.url;
                    foreach (string weatherItem in weather)
                    {
                        if (descriptionTags.Any(weatherItem.Contains))
                        {
                            weatherMatchesList.Add(fileUri);
                        }
                    }
                }

                int randomIndex = random.Next(weatherMatchesList.Count());
                string randomFileUri = weatherMatchesList[randomIndex];
                string weatherPhoto = $"https:{randomFileUri}";
                // Random index appended to downloaded file name to fix an AppleScript hardship where setting
                // ... the background image to the same filename wouldn't work even if the file was different
                string downloadFile = $"weatherPhoto{randomIndex}.jpg";

                // Download the image, blocks the thread while the downlodd is in progress
                webClient.DownloadFile($"{weatherPhoto}", $"{downloadFile}");

                // Downloading image delay
                await Task.Delay(100);

                string path = "/Users/grahamscanlon/Documents/sideprojects/WallpaperWeather/WallpaperWeather/setWallpaperScript.sh";
                string finder = "\"Finder\"";
                string photoFile = $"\"/Users/grahamscanlon/Documents/sideprojects/WallpaperWeather/WallpaperWeather/{downloadFile}\"";
                string command = $"osascript -e 'tell application {finder} to set desktop picture to {photoFile} as POSIX file'";

                string createText = "#!/bin/bash" + Environment.NewLine + command;
                File.WriteAllText(path, createText);

                Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = "/bin/bash";
                proc.StartInfo.Arguments = $"{path}";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.Start();

                // Setting image to wallpaper delay
                await Task.Delay(3000);

                Console.WriteLine("\n\nAll operations completed successfully, will resume in an hour\n\n");
                // Wait one hour before re-executing
                await Task.Delay(3600000);
            }
        }

        private static async Task GetInput()
        {
            bool exit = false;
            Action work = () =>
            {
                do
                {
                    var key = Console.ReadKey(true);
                    if (key.KeyChar == 'q')
                    {
                        Console.WriteLine("Now Exiting...");
                        exit = true;
                    }
                } while (!exit);
            };
            await Task.Run(work);
        }
    }
}
