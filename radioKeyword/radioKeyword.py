#!/usr/bin/python3
# -*- coding: utf-8 -*-

# DISCLAIMER
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import unittest
import os
import requests
import subprocess
import bandwidth
import speech_recognition as sr
from datetime import datetime, date
from twilio.rest import Client
from time import time
import secret

# Use at your own risk
#
# I tested the program one part at a time, never fully
# ... so there might be a few bugs still


class RadioKeyword:
    def __init__(self):
        self.api = bandwidth.client('catapult', secret.bandwidthU(), secret.bandwidthT(), secret.bandwidthKey())
        self.BANDWIDTH_NUMBER = secret.bandwidthNumber()
        TWILIO_ACCOUNT_SID = secret.twilioSID()
        TWILIO_AUTH_TOKEN = secret.twilioAuthToken()
        self.client = Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
        self.TWILIO_NUMBER = secret.twilioNumber()
        self.WIT_AI_KEY = secret.witKey()
        self.HOUNDIFY_CLIENT_ID = secret.houndifyID()
        self.HOUNDIFY_CLIENT_KEY = secret.houndifyKey()

    def record(self):
        today = str(datetime.fromtimestamp(time()))
        # KLOU 103.3 St. Louis
        stream_url = 'http://94.23.15.114:8006/stream'
        print('Recording... {0} | ctrl-c to break | {1}'.format(stream_url, today), flush=True)
        # Connect to the radio stream
        r = requests.get(stream_url, stream=True)
        ts = int(str(time())[:10])  # Timestamp
        frames = []

        try:
            for block in r.iter_content(1024):  # Chunk size
                frames.append(block)
                # Record for 300 seconds, 5 minutes
                if int(str(time())[:10]) - ts > 300:
                    today = str(datetime.fromtimestamp(time()))
                    print('Successfully recorded 5 min of audio {0}'.format(today), flush=True)
                    break
        except KeyboardInterrupt:
            today = str(datetime.fromtimestamp(time()))
            print('Recording stopped, some audio was recorded {0}'.format(today))
            pass

        # Write audio to a file: stream.mp3
        with open('stream.mp3', 'wb') as f:
            f.write(b''.join(frames))
            today = str(datetime.fromtimestamp(time()))
            print('Successfully wrote audio to .mp3 {0}'.format(today))

        # Convert .mp3 to .wav
        pathy = (os.path.join(os.path.dirname(os.path.realpath(__file__)), "stream.mp3"))
        subprocess.call([r'C:\ffmpeg-20170418-6108805-win64-static\bin\ffmpeg', '-i', pathy, 'stream.wav'])
        today = str(datetime.fromtimestamp(time()))
        print('Converted to .wav {0}'.format(today))

        transcription = self.transcription()
        today = str(datetime.fromtimestamp(time()))
        print('Transcribed {0}'.format(today), flush=True)
        return transcription

    def transcription(self):
        transcription = None, None
        today = str(datetime.fromtimestamp(time()))
        print('Transcribing audio {0}'.format(today), flush=True)

        # Using Speech Recognition module
        AUDIO_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), "stream.wav")

        # use the audio file as the audio source
        r = sr.Recognizer()
        # What noise level constitutes voices vs background noise, default 300
        r.energy_threshold = 1000
        # energy_threshold allowed to change while processing file
        r.dynamic_energy_threshold = True

        with sr.AudioFile(AUDIO_FILE) as source:
            # Read the entire audio file
            audio = r.record(source)

        '''
        # Recognize speech using Sphinx
        try:
            transcription = (r.recognize_sphinx(audio)), 'SPHINX'
        except sr.UnknownValueError:
            print("Sphinx could not understand audio")
        except sr.RequestError as e:
            print("Sphinx error; {0}".format(e))
        '''

        '''
        # Recognize speech using Wit.ai
        try:
           transcription = (r.recognize_wit(audio, key=self.WIT_AI_KEY)), 'WIT'
        except sr.UnknownValueError:
            print("Wit.ai could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Wit.ai service; {0}".format(e))
        '''

        '''
        # Recognize speech using Houndify
        try:
            transcription = (r.recognize_houndify(audio, client_id=self.HOUNDIFY_CLIENT_ID, client_key=self.HOUNDIFY_CLIENT_KEY)), 'HOUNDIFY'
        except sr.UnknownValueError:
            print("Houndify could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Houndify service; {0}".format(e))
        '''

        # So ffmpeg doesn't prompt to overwrite the audio file
        os.remove(AUDIO_FILE)

        return transcription

    def on20AfterTheHour(self):
        print('Sleeping...')
        while True:
            today = str(datetime.fromtimestamp(time())).replace(
                '-', ' ').replace(':', ' ').replace('.', ' ').split(' ')
            todayyear = int(today[0])
            todaymonth = int(today[1])
            todayday = int(today[2])
            todayhour = int(today[3])
            todayminute = int(today[4])

            if date(todayyear, todaymonth, todayday).weekday() <= 4:  # 0 - 4 == Mon - Fri
                if 7 <= todayhour <= 19:  # From 7am to 7pm
                    if 19 <= todayminute <= 20:  # Minute 19 or 20 of the hour
                        keypara, service = self.record()
                        print(keypara, flush=True)
                        self.text(keypara, service)
                        today = str(datetime.fromtimestamp(time()))
                        print('Sleeping... {0}'.format(today), flush=True)
                    elif todayminute < 19:
                        time.sleep(54 * (19 - todayminute))  # Till about minute 19
                    elif todayminute > 20:
                        time.sleep(1140)  # 19 min
                elif todayhour > 19:
                    time.sleep(3600 * (30 - todayhour))  # Till 6am
                elif todayhour < 6:
                    time.sleep(3600 * (6 - todayhour))  # Till 6am
                elif todayhour == 6:
                    time.sleep(1140)  # 19 min
            else:  # Weekends
                if 7 <= todayhour <= 19:  # From 7am to 7pm
                    time.sleep(39600)  # Half one day - one hour
                elif todayhour > 19:
                    time.sleep(3600 * (30 - todayhour))  # Till 6am
                elif todayhour < 7:
                    time.sleep(86400)  # One day

    def text(self, keypara, service):
        today = str(datetime.fromtimestamp(time()))
        if keypara is None:
            print('Audio could not be recognized {0}'.format(today), flush=True)
            # Limited to 100 cred/day, each request is about 75 cred
            if service == 'HOUNDIFY':
                print('Sleeping... {0}'.format(today), flush=True)
                time.sleep(43200)  # Half day
            return

        keypara = keypara.split(' ')
        keyword = None

        while 'keyword' in keypara:
            indexkw = keypara.index('keyword')
            if keypara[indexkw + 1] == 'is':
                keyword = keypara[indexkw + 2]
                break
            else:
                del keypara[indexkw]

        if keyword is None:
            print('Could not find keyword {0}'.format(today), flush=True)
            # Limited to 100 cred/day, each request is about 75 cred
            if service == 'HOUNDIFY':
                print('Sleeping... {0}'.format(today), flush=True)
                time.sleep(43200)  # Half day
            return

        '''
        # Send text using Twilio
        message = self.client.api.account.messages.create(to="+1200200", from_=self.TWILIO_NUMBER, body=keyword) # Possibly to="200200"
        print (message.sid, "Message sent {0}".format(today))
        '''

        '''
        # Send text using Bandwidth
        message_id = self.api.send_message(from_ = self.BANDWIDTH_NUMBER, to = '+1200200', text = keyword)  # Possibly to="200200"
        print(message_id, "Message sent {0}".format(today))
        '''

        # Limited to 100 cred/day, each request is about 75 cred
        if service == 'HOUNDIFY':
            print('Sleeping... {0}'.format(today), flush=True)
            time.sleep(43200)  # Half day
        return


'''
# Testing
class MyTest(unittest.TestCase):
    def test_me(self):
        r = RadioKeyword()
        self.assertIsNone(r.on20AfterTheHour()) #OK
'''

if __name__ == '__main__':
    # unittest.main()
    RadioKeyword().on20AfterTheHour()
