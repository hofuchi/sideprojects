#!/usr/bin/python2.7
from twilio.rest import TwilioRestClient
import re
import httplib2
import praw
import time
import os

r = praw.Reddit('News monitor')
r.login()
already_done = []
multireddit = r.get_multireddit([username], 'usandmissourinews')

while True:
    os.system("clear")
    print "us_and_missouri_news in progress"
    submissions = multireddit.get_new(limit=5)
    for submission in submissions:
        if submission.id not in already_done:
            if "imgur" not in submission.url:
                TITLE = "TITLE: ", submission.title, "\n"
                PERMALINK = "PERMALINK: ", submission.permalink, "\n"
                URL = "URL: ", submission.url
                already_done.append(submission.id)
                TITLEURLS = TITLE + PERMALINK + URL
                TITLEURLS = ''.join(TITLEURLS)

                account_sid = [twilio_sid]  # Your Account SID from www.twilio.com/console
                auth_token = [twilio_auth_token]  # Your Auth Token from www.twilio.com/console

                client = TwilioRestClient(account_sid, auth_token)
                message = client.messages.create(
                    to="1234567890",
                    from_="+1234567890",
                    body=TITLEURLS)
        time.sleep(60)
    time.sleep(3540)
